using Newtonsoft.Json;
using Scriban;
using Scriban.Parsing;
using Scriban.Runtime;
using SdkGen.UnrealTypes;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace SdkGen
{
    public struct GameInfo
    {
        public string Name;
        public string ShortName;
        public string Version;
    }

    public struct EngineInfo
    {
        public long ProcessEvent;
        public long ProcessInternal;
        public long CallFunction;

        public long GlobalObjects;
        public long GlobalNames;
        public long GlobalNatives;

        public int ProcessEventIndex;
    }

    public class SdkGenerator
    {
        public static string GameInfoFileName = "GameInfo.json";
        public static string EngineInfoFileName = "EngineInfo.json";

        public GameInfo GameInfo { get; private set; }

        public EngineInfo EngineInfo { get; private set; }

        public string ExportPath { get; private set; }
        public string OutputPath { get; private set; }

        public List<UPackage> Packages { get; private set; }

        protected Template ClassHeaderTpl;
        protected Template ClassSourceTpl;
        protected Template ClassReflectionTpl;
        protected Template StructHeaderTpl;

        protected static TemplateLoader TemplateLoader => new TemplateLoader();

        public SdkGenerator(string ExportPath, string OutputPath) 
        { 
            this.ExportPath = ExportPath;
            this.OutputPath = OutputPath;
            this.Packages = new List<UPackage> { };

            ParseTemplates();

            if(ValidateExports())
            {
                LoadGameInfo();
                LoadEngineInfo();
                LoadPackages();
            }
        }

        protected void ParseTemplates()
        {
            string templateDirName = "Resources/Templates";
            string templateDirPath = Path.Combine(Environment.CurrentDirectory, templateDirName);

            try
            {
                ClassHeaderTpl = Template.Parse(File.ReadAllText(Path.Combine(templateDirPath, "ClassHeader.h")));
                ClassReflectionTpl = Template.Parse(File.ReadAllText(Path.Combine(templateDirPath, "ClassReflection.h")));
                ClassSourceTpl = Template.Parse(File.ReadAllText(Path.Combine(templateDirPath, "ClassSource.cpp")));
                StructHeaderTpl = Template.Parse(File.ReadAllText(Path.Combine(templateDirPath, "StructHeader.h")));
            } catch (Exception ex)
            {
                Log.Error(ex, "Failed to parse templates");
            }
        }

        protected bool ValidateExports()
        {
            // check ExportPath
            if(!Directory.Exists(ExportPath))
            {
                Log.Error("{ExportPath} is not a valid directory", ExportPath); 
                return false;
            }

            // check for GameInfo
            if(!File.Exists(Path.Join(ExportPath, GameInfoFileName)))
            {
                Log.Error("Export path {ExportPath} does not contain {GameInfoFile}", ExportPath, GameInfoFileName);
                return false;
            }

            // check for EngineInfo
            if(!File.Exists(Path.Join(ExportPath, EngineInfoFileName)))
            {
                Log.Error("Export path {ExportPath} does not contain {EngineInfoFile}", ExportPath, EngineInfoFileName);
                return false;
            }

            return true;
        }

        protected void LoadGameInfo()
        {
            try
            {
                var gameInfoStr = File.ReadAllText(Path.Join(ExportPath, GameInfoFileName));
                GameInfo = JsonConvert.DeserializeObject<GameInfo>(gameInfoStr);
            } catch (JsonException ex)
            {
                Log.Error(ex, "Invalid {GameInfoFile}", GameInfoFileName);
            }
        }

        protected void LoadEngineInfo()
        {
            try
            {
                var engineInfoStr = File.ReadAllText(Path.Join(ExportPath, EngineInfoFileName));
                EngineInfo = JsonConvert.DeserializeObject<EngineInfo>(engineInfoStr);
            } catch (JsonException ex)
            {
                Log.Error(ex, "Invalid {EngineInfoFile}", EngineInfoFileName);
            }
        }

        protected void LoadPackages(UPackage? Parent = null)
        {
            string[] dirs = Directory.GetDirectories(Parent == null ? ExportPath : Parent.InputPath, "*", SearchOption.TopDirectoryOnly);

            foreach (string dir in dirs) 
            {
                var dirInfo = new DirectoryInfo(dir);
                UPackage package = new UPackage(dirInfo.Name, dir, null);
                LoadPackages(package);

                if (Parent == null)
                    Packages.Add(package);
                else
                    Parent.Packages.Add(package);
            }
        }

        public void ProcessPackages()
        {
            if(!Directory.Exists(OutputPath))
                Directory.CreateDirectory(OutputPath);

            foreach(var package in Packages)
            {
                if (!Directory.Exists(Path.Join(OutputPath, package.Name)))
                    Directory.CreateDirectory(Path.Join(OutputPath, package.Name));

                string[] packageTypeDefs = Directory.GetFiles(package.InputPath, "*.json", SearchOption.TopDirectoryOnly);

                foreach(var typeDefFile in packageTypeDefs)
                {
                    var typeDefFileName = Path.GetFileName(typeDefFile);
                    if(typeDefFileName.StartsWith("F"))
                    {
                        var strDef = ParseStructDef(typeDefFile);
                        if(strDef != null)
                        {
                            ProcessStructDef(strDef);
                        }
                    } else
                    {
                        var clsDef = ParseClassDef(typeDefFile);
                        if(clsDef != null)
                        {
                            ProcessClassDef(clsDef);
                        }
                    }
                }
            }

            Log.Information("Formatting header files...");
            
            //Process.Start("CMD.exe", "/C clang-format -i --glob=sdk/**/*.h").WaitForExit();

            Log.Information("Formatting source files...");
            //Process.Start("CMD.exe", "/C clang-format -i --glob=sdk/**/*.cpp").WaitForExit();
        }

        protected UStruct? ParseStructDef(string file)
        {
            UStruct? str = null;
            try
            {
                var strDefStr = File.ReadAllText(file);
                str = JsonConvert.DeserializeObject<UStruct>(strDefStr);
            } catch (JsonException ex)
            {
                Log.Error(ex, "Failed to parse struct definition {StructDefFile}", file);
            }

            return str;
        }

        protected UClass? ParseClassDef(string file)
        {
            UClass? cls = null;
            try
            {
                var clsDefStr = File.ReadAllText(file);
                cls = JsonConvert.DeserializeObject<UClass>(clsDefStr);
            } catch (JsonException ex)
            {
                Log.Error(ex, "Failed to parse class definition {ClassDefFile}", file);
            }

            return cls;
        }

        protected void ProcessStructDef(UStruct str)
        {
            Log.Information("Processing struct {StructName}", str.Name);

            string outFilePath = Path.Join(OutputPath, str.NameSpace.Replace(".", "/"), str.Name + ".h");

            var scriptObj = new ScriptObject
            {
                { "TypeInfo", str },
                { "BaseIncludes", new List<string>{ } },
                { "ForwardDeclarations", null },
            };

            RenderTemplateToFile(StructHeaderTpl, scriptObj, outFilePath);
        }

        protected void ProcessClassDef(UClass cls)
        {
            Log.Information("Processing class {ClassName}", cls.Name);

            var baseIncludes = cls.BuildIncludeList;

            var scriptObj = new ScriptObject
            {
                {"TypeInfo", cls},
                {"BaseIncludes", cls.BuildIncludeList },
                {"ForwardDeclarations", null }
            };

            string outFilePath = Path.Join(OutputPath, cls.NameSpace.Replace(".", "/"), cls.Name + ".h");
            RenderTemplateToFile(ClassHeaderTpl, scriptObj, outFilePath);

            if(cls.Functions.Count > 0)
            {
                string outClassReflectionPath = Path.Join(OutputPath, cls.NameSpace.Replace(".", "/"), cls.Name + "Reflection.h");
                RenderTemplateToFile(ClassReflectionTpl, scriptObj, outClassReflectionPath);

                string outClassSourcePath = Path.Join(OutputPath, cls.NameSpace.Replace(".", "/"), cls.Name + ".cpp");
                RenderTemplateToFile(ClassSourceTpl, scriptObj, outClassSourcePath);
            }
        }

        protected TemplateContext GetTemplateContext(ScriptObject scriptObject)
        {
            var tplContext = new TemplateContext();
            tplContext.PushGlobal(scriptObject);
            tplContext.TemplateLoader = TemplateLoader;
            tplContext.MemberRenamer = (type) => type.Name;
            tplContext.LoopLimit = 0;
            return tplContext;
        }

        protected void RenderTemplateToFile(Template tpl, ScriptObject vars, string outputPath)
        {
            var result = tpl.Render(GetTemplateContext(vars));
            File.WriteAllText(outputPath, result);
        }
    }
}
