using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SdkGen.UnrealTypes
{
    public class UPackage
    {
        public string Name { get; set; }
        public UPackage? Parent { get ; set; }
        public string InputPath { get; set; }

        public List<UPackage> Packages { get; protected set; } = new List<UPackage>();

        public UPackage()
        {
            Name = "";
            InputPath = "";
            Parent = null;
        }

        public UPackage(string Name, string InputPath, UPackage? Parent = null)
        { 
            this.Name = Name;
            this.InputPath = InputPath;
            this.Parent = Parent;
        }
    }

    public class UProperty
    {
        public string Name { get; set; }
        public string Type { get; set; }

        public int Offset { get; set; }
        public List<string> PropertyFlags { get; set; }

        public int PropertySize { get; set; }

        public bool IsBitField => Type == "bool";

        public bool IsParm => PropertyFlags.Contains("Parm");
        public bool IsOutParm => PropertyFlags.Contains("OutParm");
        public bool IsOptionalParm => PropertyFlags.Contains("OptionalParm");
        public bool IsReturnParm => PropertyFlags.Contains("ReturnParm");

        public string Signature => Type.Replace(".", "::") + (IsOutParm ? IsOptionalParm ? "*" : "&" : "") + " " + Name;

        public string Declaration => Signature + (IsOptionalParm ? " = 0" : "");

        public UProperty(string Name, string Type, int Offset, List<string> PropertyFlags) 
        {
            this.Name = Name;
            this.Type = Type;
            this.Offset = Offset;
            this.PropertyFlags = PropertyFlags;
        }
    }

    public class UFunction
    {
        public string Name { get; set; }
        public int ObjectIndex { get; set; }
        public List<string> FunctionFlags { get; set; }
        public List<UProperty> Parameters { get; set; }
        public List<UProperty> Locals { get; set; }
        //public UProperty? Returns { get; set; }

        public int NumParams { get; set; }
        public int ParamSize { get; set; }
        public long Pointer { get; set; }
        public int RepOffset { get; set; }
        public string ReturnType { get; set; }
        public int ReturnValueOffset { get; set; }
        public short iNative { get; set; }

        public bool IsNative => FunctionFlags.Contains("Native") || iNative > 0;
        public bool IsStatic => FunctionFlags.Contains("Static");
        public bool IsEvent => FunctionFlags.Contains("Event");

        public List<UProperty> Arguments => ReturnType == "void" ? Parameters : Parameters.Where(p => !p.IsReturnParm).ToList();
    }

    public class UObject
    {
        public string Name { get; set; }
        public string PackageName { get; set; }
        public string ObjectInternalInteger { get; set; }
        public List<string> ObjectFlags { get; set; }

        public bool IsActor => Name.StartsWith("A");
        public bool IsClass => Name.StartsWith("U");
        public bool IsStruct => Name.StartsWith("F");
    }

    public class UStruct : UObject
    {
        public string ParentName { get; set; }
        public int Size { get; set; }
        public int StructSize { get; set; }

        public List<UProperty> Properties { get; set; }
        public Dictionary<string, List<string>> Enums { get; set; }
        public Dictionary<string, string> Constants { get; set; }
        public List<string> States { get; set; }

        public List<UFunction> Functions { get; set; } = new List<UFunction>();

        public List<UFunction> PublicFunctions 
            => Functions.Where(func => func.FunctionFlags.Contains("Public"))
                .OrderByDescending(func => func.FunctionFlags.Contains("Static")).ToList();
        public List<UFunction> PrivateFunctions
            => Functions.Where(func => func.FunctionFlags.Contains("Private"))
                .OrderByDescending(func => func.FunctionFlags.Contains("Static")).ToList();
        public List<UFunction> ProtectedFunctions
            => Functions.Where(func => func.FunctionFlags.Contains("Protected"))
                .OrderByDescending(func => func.FunctionFlags.Contains("Static")).ToList();

        public string NameSpace => PackageName;

        public List<UProperty> PaddedProperties
        {
            get
            {
                List<UProperty> paddedProps = new List<UProperty>();

                int paddings = 0;
                for (int i = 0; i < Properties.Count; i++)
                {
                    var prop = Properties.ElementAt(i);
                 

                    if (i == 0 && prop.Offset > Size - StructSize)
                    {
                        paddedProps.Add(new UProperty("UnknownData_" + paddings + "[" + (prop.Offset - (Size - StructSize)) + "]", "char", 0, new List<string>()));
                        paddings++;
                    }

                    paddedProps.Add(prop);

                    if(i + 1 < Properties.Count)
                    {
                        var nextProp = Properties.ElementAt(i + 1);
                        int diff = nextProp.Offset - (prop.Offset + prop.PropertySize);

                        if (diff > 0)
                        {
                            paddedProps.Add(new UProperty("UnknownData_" + paddings + "[" + diff + "]", "char", prop.Offset + prop.PropertySize, new List<string>()));
                            paddings++;
                        }
                    } 
                    else
                    {
                        var diff = Size - (prop.Offset + prop.PropertySize);
                        if (diff > 0)
                            paddedProps.Add(new UProperty("UnknownData_" + paddings + "[" + diff + "]", "char", prop.Offset + prop.PropertySize, new List<string>()));
                    }
                }

                return paddedProps;
            }
        }

        public List<string> BuildIncludeList
        {
            get
            {
                HashSet<string> props = new HashSet<string>();

                var propTypeBaseName = (string t) => t
                    .Replace("Core.TArray<", "").Replace(">", "")
                    .Replace("Core.TMap<", "")
                    .Replace(".", "/").Replace("*", "");

                var propTypeInclude = (string t) => "../" + propTypeBaseName(t) + ".h";

                props = Properties.Where(p => p.Type.Contains(".")).Select(p => propTypeInclude(p.Type)).ToHashSet();
                if(Functions != null)
                    props.Concat(Functions.SelectMany(f => f.Parameters).Where(p => p.Type.Contains(".")).Select(p => propTypeInclude(p.Type)))
                    .Concat(Functions.Where(f => f.ReturnType.Contains(".")).Select(f => propTypeInclude(f.ReturnType)))
                    .ToHashSet();

                if (ParentName != "")
                    props.Add(propTypeInclude(ParentName));

                return props.ToList();
            }
        }
    }

    public class UClass : UStruct
    {
        public List<string> ClassFlags { get; set; }
        public int DefaultClassIndex { get; set; }
    }
}
