﻿// See https://aka.ms/new-console-template for more information

using Serilog;

namespace SdkGen
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger();

            if(args.Length == 0)
            {
                Log.Error("Please specify the export path!");
                return;
            }

            SdkGenerator sdkGen = new SdkGenerator(args[0], "./sdk");

            sdkGen.ProcessPackages();
        }
    }
}