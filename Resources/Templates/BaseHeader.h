#pragma once
{{~ for BaseInclude in BaseIncludes ~}}
#include "{{BaseInclude}}"
{{~ end ~}}
{{~ for Declaration in ForwardDeclarations ~}}
{{ Declaration.Type }} {{ Declaration.Name }};
{{~ end ~}}
{{~ for Constant in TypeInfo.Constants ~}}
#define {{ Constant.Key }} {{ Constant.Value }}
{{~ end ~}}