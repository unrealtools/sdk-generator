#include "{{TypeInfo.Name}}.h"

using namespace {{TypeInfo.NameSpace}};

{{~ for Function in TypeInfo.Functions ~}}
{{ Function.ReturnType | string.replace "." "::" }} {{TypeInfo.NameSpace}}::{{TypeInfo.Name}}::{{Function.Name}}({{for Arg in Function.Arguments}}{{Arg.Signature}}{{if !for.last}}, {{end}}{{end}})
{
	return UERefl({{TypeInfo.NameSpace}}::{{TypeInfo.Name}}::{{Function.Name}})::Execute({{for Arg in Function.Arguments}}{{Arg.Name}}{{if !for.last}}, {{end}}{{end}})
};

{{~ end ~}}