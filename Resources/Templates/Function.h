/**
* 
*	{{ $.Function.FunctionFlags | array.join " | " }}
*/ 
{{ if $.Function.IsStatic }}static {{end}}{{ $.Function.ReturnType | string.replace "." "::" }} {{ $.Function.Name }}({{for Arg in $.Function.Arguments}}{{Arg.Declaration}}{{if !for.last}}, {{end}}{{end}});
