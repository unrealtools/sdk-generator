{{~ for Enum in $.Enums ~}}
enum class {{ Enum.Key }} : unsigned char
{
	{{~ for EnumValue in Enum.Value ~}}
	{{ EnumValue }},
	{{~ end ~}}
};

{{~ end ~}}