{{~ include 'BaseHeader.h' ~}}

namespace {{ TypeInfo.NameSpace }}
{
	{{ include 'Enums.h' Enums:TypeInfo.Enums }}

	/**
	*	ObjectFlags: {{ TypeInfo.ClassFlags | array.join " | " }}
	*	ClassFlags: {{ TypeInfo.ObjectFlags | array.join " | " }}
	*	Size: {{ TypeInfo.StructSize | math.format "X" }}
	*/
	class {{ TypeInfo.Name }} {{~ if !string.empty TypeInfo.ParentName ~}} : public {{ TypeInfo.ParentName | string.replace "." "::" }}{{~ end ~}}
	{
	{{~ for Property in TypeInfo.PaddedProperties ~}}
	{{~ if for.first ~}}
	public:
	{{~ end ~}}
		/** 
		* PropertyFlags: {{ Property.PropertyFlags | array.join " | " }}
		* Offset: {{ Property.Offset | math.format "X" }}
		*/
		{{ Property.Type | string.replace "." "::" }} {{ Property.Name }}{{~ if Property.IsBitField ~}} : 1{{~ end ~}};
	{{~ end ~}}

	{{~ for Func in TypeInfo.PublicFunctions ~}}
	{{~ if for.first ~}}
	public:
	{{~ end ~}}
		{{ include 'Function.h' Function:Func }}
	{{~ end ~}}

	{{~ for Func in TypeInfo.ProtectedFunctions ~}}
	{{~ if for.first ~}}
	protected:
	{{~ end ~}}
		{{ include 'Function.h' Function:Func }}
	{{~ end ~}}

	{{~ for Func in TypeInfo.PrivateFunctions ~}}
	{{~ if for.first ~}}
	private:
	{{~ end ~}}
		{{ include 'Function.h' Function:Func }}
	{{~ end ~}}

	}
}