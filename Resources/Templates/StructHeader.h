{{~ include 'BaseHeader.h' ~}}

namespace {{ TypeInfo.NameSpace }}
{
	{{ include 'Enums.h' Enums:TypeInfo.Enums }}

	/**
	*	ObjectFlags: {{ TypeInfo.ClassFlags | array.join " | " }}
	*/
	struct {{ TypeInfo.Name }}
	{
	{{~ for Property in TypeInfo.Properties ~}}
		{{ Property.Type | string.replace "." "::" }} {{ Property.Name }}{{~ if Property.IsBitField ~}} : 1{{~ end ~}};
	{{~ end ~}}
	};
}