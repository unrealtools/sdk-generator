#include "{{TypeInfo.Name}}Reflection.h"

namespace FnInfo
{
	namespace {{TypeInfo.NameSpace}}::{{TypeInfo.Name}}
	{
		{{~ for Func in TypeInfo.Functions ~}}
		struct {{Func.Name}}
		{{~if Func.IsStatic ~}}
			: UnrealStaticFunction<{{Func.ObjectIndex}}, ::{{TypeInfo.NameSpace}}::{{TypeInfo.Name}}, decltype(&::{{TypeInfo.NameSpace}}::{{TypeInfo.Name}}::{{Func.Name}})>
		{{~ else ~}}
			: UnrealFunction<{{Func.ObjectIndex}}, decltype(&::{{TypeInfo.NameSpace}}::{{TypeInfo.Name}}::{{Func.Name}})>
		{{~ end ~}}
			{};
		{{~ end ~}}
	}

	namespace {{TypeInfo.Name}} = {{TypeInfo.NameSpace}}::{{TypeInfo.Name}};
}