#pragma once
#include <tuple>
#include <type_traits>

#define UEREFL(ns) UEREFL:: ns

/**
 * @brief Wrapper for info about a UFunction object.
 * @tparam declaration function type declaration
 * @tparam index object index
 */
template<int index, typename declaration>
struct UFunctionInfo;

/**
 * @brief Infos about a lose (non-owned) UFunction object.
 * @tparam return_type return type
 * @tparam ...args argument list
 * @tparam index object index
 */
template<int index, typename return_type, typename ...args>
struct UFunctionInfo<index, return_type(args...)>
{
    // object index
    constexpr static int Index = index;
    // return type
    using ReturnType = return_type;
    
    // argument list (as tuple)
    using Arguments = std::tuple<args...>;
    // number of arguments
    constexpr static int ArgumentCount = std::tuple_size<Arguments>::value;
    
    // function type declaration
    using Declaration = return_type(args...);

    // pretty function string
    inline static std::string Syntax = std::format("{}#{}", type_name<Declaration>(), std::to_string(Index));
};

/**
 * @brief 
 * @tparam uclass 
 * @tparam return_type 
 * @tparam ...args 
 * @tparam index 
 */
template<int index, typename uclass, typename return_type, typename ...args>
struct UFunctionInfo<index, return_type (uclass::*)(args...)>
    : UFunctionInfo<index, return_type(args...)>
{
    using ClassType = uclass;
    using Declaration = return_type(uclass::*)(args...);
    inline static std::string Syntax = std::format("{}#{}", type_name<Declaration>(), std::to_string(index));
};

template<int index, typename uclass, typename return_type, typename ...args>
struct UStaticFunctionInfo 
    : UFunctionInfo<index, return_type(args...)>
{
    using ClassType = uclass;
    using Declaration = return_type(args...);
    
    inline static std::string Syntax = std::format("{}#{}", type_name<Declaration>(), std::to_string(index));
};