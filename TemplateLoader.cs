using Scriban;
using Scriban.Parsing;
using Scriban.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SdkGen
{
    public class TemplateLoader : ITemplateLoader
    {
        public string GetPath(TemplateContext context, SourceSpan sourceSpan, string templateName)
        {
            return Path.Combine(Environment.CurrentDirectory, "Resources", "Templates", templateName);
        }

        public string Load(TemplateContext context, SourceSpan callerSpan, string templatePath)
        {
            return File.ReadAllText(templatePath, Encoding.UTF8);
        }

        public ValueTask<string> LoadAsync(TemplateContext context, SourceSpan callerSpan, string templatePath) 
            => new ValueTask<string>(File.ReadAllTextAsync(templatePath, Encoding.UTF8));
    }
}
